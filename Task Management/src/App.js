import React from 'react';
import { BrowserRouter as Router, Route } from "react-router-dom";
import "bootstrap/dist/css/bootstrap.min.css";
import "./App.css";
 
import Navbar from "./components/navbar.component"
import TaskList from "./components/tasks-list.component";
import EditTask from "./components/edit-task.component";
import TaskDetails from "./components/task-details.component";
import CreateTask from "./components/create-task.component";

function App() {
  return (
    <Router>
      <div className="container">
      <Navbar />
      <br/>
      <Route path="/" exact component={TaskList} />
      <Route path="/edit/:id" component={EditTask} />
      <Route path="/create" component={CreateTask} />
      <Route path="/details/:id" component={TaskDetails} />
      </div>
    </Router>
  );
 }
 
export default App;