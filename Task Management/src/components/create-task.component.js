import React, { Component } from 'react';
import DatePicker from 'react-datepicker';
import "react-datepicker/dist/react-datepicker.css";
import axios from 'axios';

export default class CreateTask extends Component {
  constructor(props) {
    super(props);

      this.onChangeTaskname = this.onChangeTaskname.bind(this);
      this.onChangeDescription = this.onChangeDescription.bind(this);
      this.onChangeStatus = this.onChangeStatus.bind(this);
      this.onChangeAssignedto = this.onChangeAssignedto.bind(this);
      this.onChangeStartdate = this.onChangeStartdate.bind(this);
      this.onChangeExpectedenddate = this.onChangeExpectedenddate.bind(this);
      this.onChangeFilepath = this.onChangeFilepath.bind(this);
      this.onSubmit = this.onSubmit.bind(this);

      this.state = {
        taskname: '',
        description: '',
        status: '',
        assignedto: '',
        startdate: new Date(),
        expectedenddate: new Date(),
        filepath: '',
        users: []
      }
  }

  onChangeTaskname(e) {
    this.setState({
      taskname: e.target.value
    });
  }
  onChangeDescription(e) {
    this.setState({
      description: e.target.value
    });
  }
  onChangeStatus(e) {
    this.setState({
      status: e.target.value
    });
  }
  onChangeAssignedto(e) {
    this.setState({
      assignedto: e.target.value
    });
  }
  

  onChangeStartdate(date) {
    this.setState({
      startdate: date
    });
  }

  onChangeExpectedenddate(date) {
    this.setState({
      expectedenddate: date
    });
  }

  onChangeFilepath(e) {
    this.setState({
      filepath: e.target.value
    });
  }
  
  onSubmit(e) {
    e.preventDefault();
    const task = {
      taskname: this.state.taskname,
      description: this.state.description,
      status: this.state.status,
      assignedto: this.state.assignedto,
      startdate: this.state.expectedenddate,
      expectedenddate: this.state.expectedenddate,
      filepath: this.state.filepath,
    };

  console.log(task);

  axios.post('http://localhost:5001/tasks/add', task)
  .then(res => console.log(res.data));

  window.location = '/';
  }
  render() {
    return (
      <div>
        <h3>Create New Task</h3>
        <form onSubmit={this.onSubmit}>
        <div className="form-group"> 
           <label>TaskName: </label>
           <input  type="text"
                required
                className="form-control"
                value={this.state.taskname}
                onChange={this.onChangeTaskname}
                />
          </div>
          <div className="form-group"> 
            <label>Description: </label>
            <input  type="text"
                required
                className="form-control"
                value={this.state.description}
                onChange={this.onChangeDescription}
                />
          </div>
          <div className="form-group">
            <label>Status: </label>
            <select type="userInput"
                required
                className="form-control"
                value={this.state.status}
                onChange={this.onChangeStatus}>
                  <option status=" "> Select</option>
                  <option status="New/Unassigned">New/UnAssigned</option>
                  <option status="In Proress">In Progress</option>
                  <option status="Closed">Closed</option>
               </select>
               
          </div>
          <div className="form-group">
            <label>AssignedTo: </label>
            <div>
            <input  type="text"
                required
                className="form-control"
                value={this.state.assignedto}
                onChange={this.onChangeAssignedto}
                />
            </div>
          </div>
          <div className="form-group">
            <label>StartDate: </label>
            <div>
              <DatePicker
                selected={this.state.startdate}
                onChange={this.onChangeStartdate}
              />
            </div>
          </div>
          <div className="form-group">
            <label>ExpectedEndDate: </label>
            <div>
              <DatePicker
                selected={this.state.expectedenddate}
                onChange={this.onChangeExpectedenddate}
              />
            </div>
          </div>
          <div className="form-group">
            <label>FilePath: </label> 
            <input  type="text"
                required
                className="form-control"
                value={this.state.filepath}
                onChange={this.onChangeFilepath}
                />
          </div>

          <div className="form-group">
            <input type="submit" value="Create Task" className="btn btn-primary" />
          </div>
        </form>
      </div>
    )
  }
}