import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import axios from 'axios';

const Task = props => (
    <tr>
      <td>{props.task.taskname}</td>
      <td>{props.task.description}</td>
      <td>{props.task.status}</td>
      <td>{props.task.assignedto}</td>
      <td>{props.task.startdate.substring(0,10)}</td>
      <td>{props.task.expectedenddate.substring(0,10)}</td>
      <td>{props.task.filepath}</td>
      <td>
      <Link to={"/edit/"+props.task._id}>edit</Link> | <a href="#" onClick={() => { props.deleteTask(props.task._id) }}>delete</a>
     </td>
    </tr>
  )

export default class TaskDetails extends Component {
    constructor(props) {
        super(props);

        this.state = {tasks: []};
      }
    componentDidMount() {
        axios.get('http://localhost:5001/tasks/'+this.props.match.params.id)
        
         .then(response => {
           this.setState({ tasks: response.data });
         })
         
         .catch((error) => {
            console.log(error);
         })
         //console.log(axios.get('http://localhost:5001/tasks/'+this.props.match.params.id));    
    }

    taskList() {
      return this.state.tasks.map(currenttask => {
        return <Task task={currenttask} deleteTask={this.deleteTask} key={currenttask._id}/>;
      })
    }
    
  render() {
    const task = this.state.tasks;
    return (
        <div>
        
        
   <div className="task-card">
     <ul className="task-details-list">
       <li className="list-item">
         <div className="key">TaskName:</div>
         <div className="value">{task.taskname}</div>
       </li>
       <li className="list-item">
         <div className="key">Description</div>
         <div className="value">{task.description}</div>
       </li>
       <li className="list-item">
         <div className="key">Status</div>
         <div className="value">{task.status}</div>
       </li>
       <li className="list-item">
         <div className="key">AssignedTo</div>
         <div className="value">{task.assignedto}</div>
       </li>
       <li className="list-item">
         <div className="key">StartDate</div>
         <div className="value">{task.startdate}</div>
       </li>
       <li className="list-item">
         <div className="key">ExpectedEndDate</div>
         <div className="value">{task.expectedenddate}</div>
       </li>
       
       <li className="list-item">
         <div className="key">FilePath</div>
         <div className="value">{task.filepath}</div>
       </li>
     </ul>
   </div>
      </div>
    )
  }
}
