import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import axios from 'axios';



const Task = props => (
    <tr>
      <td><Link to={"/details/"+props.task._id}>{props.task.taskname}</Link></td>
      {/* <td>{props.task.description}</td> */}
      <td>{props.task.status}</td>  
      <td>{props.task.assignedto}</td>
      {/* <td>{props.task.startdate.substring(0,10)}</td>
      <td>{props.task.expectedenddate.substring(0,10)}</td> */}
      {/* <td>{props.task.filepath}</td> */}
      <td>
      <Link to={"/edit/"+props.task._id}>edit</Link> | <a href="#" onClick={() => { props.deleteTask(props.task._id) }}>delete</a>
     </td>
    </tr>
  )

export default class TaskList extends Component {
    constructor(props) {
        super(props);
        this.deleteTask = this.deleteTask.bind(this);
        this.state = {tasks: []};
      }
    componentDidMount() {
        axios.get('http://localhost:5001/tasks/')
         .then(response => {
           this.setState({ tasks: response.data });
         })
         .catch((error) => {
            console.log(error);
         })
    }
    deleteTask(id) {
        axios.delete('http://localhost:5001/tasks/'+id)
          .then(res => console.log(res.data));
        this.setState({
          tasks: this.state.tasks.filter(el => el._id !== id)
        })
      }
    taskList() {
        return this.state.tasks.map(currenttask => {
          return <Task task={currenttask} deleteTask={this.deleteTask} key={currenttask._id}/>;
        })
      }
  render() {
    return (
        <div>
        <h3>Logged Tasks</h3>
        <table className="table">
          <thead className="thead-light">
            <tr>
              <th>Taskname</th>
              {/* <th>Description</th> */}
              <th>Status</th>
              <th>AssignedTo</th>
              {/* <th>StartDate</th>
              <th>ExpectedEndDate</th> */}
              {/* <th>Filepath</th> */}
              <th>Actions</th>
            </tr>
          </thead>
          <tbody>
            { this.taskList() }
          </tbody>
        </table>
      </div>
    )
  }
}
