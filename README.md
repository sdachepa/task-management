# Task Management

Task Management Tool is an application developed using MERN to manage different tasks in an organization or workplace with the status of the workdone.



### How do I get set up?

#### Tools and Technologies:     
Install below mentioned tools:    
•	Node.js    
•	MongoDB    
•	Visual Studio Code    
•	Postman    
•	React is the technology used     

#### Steps to setup the project:    
*	Create a MongoDB cloud cluster with tk20 account   
•	In cluster0  
     Connect -> Connect your appliation -> Copy the connection String    
•	Paste the link in .env file(which is in the backend folder)     
•	Replace “<password>” in the string with the account password     
*	Connect to Database:   
•	Open terminal (ctrl ~)   
•	Cd backend   
•	nodemon server    


 